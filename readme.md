# Credly .NET Core Example #

This project serves as a general reference on how one might make HTTP requests to the Credly API in the context of C#.

### Installation ###

1. Clone: `git clone git@bitbucket.org:credly/credly-dotnet.git`
2. Install packages: `dotnet restore`
3. Copy `env.json.example` to `env.json` in the project root
4. Fill in the values accordingly

### Available Commands ###

**Verify Connectivity**

`dotnet run info`

**Retrieve Authentication Token**

`dotnet run authenticate`

Follow the prompts for email and password, it will output the response to the console.

**Issue Badge**

`dotnet run issue`

Follow the prompts to retrieve an access token and use that to issue a badge.

MIT License

Copyright (c) 2018 Credly, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace credly_dotnet
{
    public class CredlyApi
    {
        private static string BaseUrl;
        private static string ApiKey;
        private static string ApiSecret;
        private static string ApiVersion = "v1.1";
        public void ApplyConfiguration(string baseUrl, string apiKey, string apiSecret)
        {
            BaseUrl = baseUrl;
            ApiKey = apiKey;
            ApiSecret = apiSecret;
        }
        private static void PrettyPrint(string output)
        {
            Console.WriteLine(
                JsonConvert.SerializeObject(
                    JsonConvert.DeserializeObject(output),
                    Formatting.Indented
                )
            );
        }
        private string DecorateRequest(HttpClient client, string endpoint, Dictionary<string, string>[] parameters = null)
        {
            client.BaseAddress = new Uri(BaseUrl);

            client.DefaultRequestHeaders.Add("X-Api-Key", ApiKey);
            client.DefaultRequestHeaders.Add("X-Api-Secret", ApiSecret);

            var requestUri = "/" + ApiVersion + "/" + endpoint;

            if (parameters != null)
            {
                foreach (Dictionary<string, string> param in parameters)
                {
                    requestUri = QueryHelpers.AddQueryString(requestUri, param);
                }
            }

            Console.WriteLine("Requested: " + requestUri);

            return requestUri;
        }
        public async Task SendGetRequest(string endpoint, Dictionary<string, string>[] parameters = null)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var requestUri = DecorateRequest(client, endpoint, parameters);
                    var response = await client.GetAsync(requestUri);

                    PrettyPrint(await response.Content.ReadAsStringAsync());
                    response.EnsureSuccessStatusCode();
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }
            }
        }
        public async Task<dynamic> SendPostRequest(string endpoint, Dictionary<string, string>[] parameters = null, KeyValuePair<string, string>[] data = null, KeyValuePair<string, string>[] additionalHeaders = null)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    if (additionalHeaders != null)
                    {
                        foreach (KeyValuePair<string, string> header in additionalHeaders)
                        {
                            client.DefaultRequestHeaders.Add(header.Key, header.Value);
                        }
                    }

                    if (data == null)
                    {
                        data = new KeyValuePair<string, string>[]{};
                    }

                    var requestUri = DecorateRequest(client, endpoint, parameters);
                    var response = await client.PostAsync(requestUri, new FormUrlEncodedContent(data));

                    var rspString = await response.Content.ReadAsStringAsync();

                    PrettyPrint(rspString);
                    response.EnsureSuccessStatusCode();

                    return Decode(rspString);
                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }

                return null;
            }
        }

        public static dynamic Decode(string jsonString)
        {
            dynamic data = JsonConvert.DeserializeObject(jsonString);
            return data;
        }
    }
}
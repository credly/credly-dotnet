﻿using System;
using System.IO;
using System.Security;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.CommandLineUtils;

namespace credly_dotnet
{
    class Program
    {
        public static IConfiguration Configuration { get; set; }

        public static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("env.json");

            Configuration = builder.Build();

            var baseUrl = Configuration["credly-api-base-url"];
            var apiKey = Configuration["credly-api-key"];
            var apiSecret = Configuration["credly-api-secret"];

            var serviceProvider = new ServiceCollection()
                .AddSingleton<CredlyApi>()
                .BuildServiceProvider();

            var credlyApi = serviceProvider.GetService<CredlyApi>();

            credlyApi.ApplyConfiguration(baseUrl, apiKey, apiSecret);

            var app = new CommandLineApplication();
            app.Name = "credly";
            app.HelpOption("-?|-h|--help");

            app.Command("info", (command) => {
                command.Description = "Verify connectivity with the Credly API";
                command.OnExecute(() => {
                    credlyApi.SendGetRequest("info").Wait();
                    return 0;
                });
            });

            app.Command("authenticate", (command) =>
            {
                command.Description = "Fetch an authentication token from credentials provided in realtime";
                command.OnExecute(() =>
                {
                    Console.WriteLine("Enter your email:");
                    var email = Console.ReadLine();
                    Console.WriteLine("Enter your password:");
                    var password = readPassword();

                    var authorizationValue = (email + ":" + password);
                    byte[] encodedAuthorization = System.Text.Encoding.UTF8.GetBytes(authorizationValue);

                    credlyApi.SendPostRequest("authenticate", null, null, new[]{
                        new KeyValuePair<string, string>("Authorization", "Basic " + Convert.ToBase64String(encodedAuthorization))
                    }).Wait();
                    return 0;
                });
            });

            app.Command("issue", (command) =>
            {
                command.Description = "Fetch an authentication token from credentials provided in realtime";
                command.OnExecute(async () =>
                {
                    Console.WriteLine("Enter your email:");
                    var email = Console.ReadLine();
                    Console.WriteLine("Enter your password:");
                    var password = readPassword();

                    var authorizationValue = (email + ":" + password);
                    byte[] encodedAuthorization = System.Text.Encoding.UTF8.GetBytes(authorizationValue);

                    var authResponse = await credlyApi.SendPostRequest("authenticate", null, null, new[]{
                        new KeyValuePair<string, string>("Authorization", "Basic " + Convert.ToBase64String(encodedAuthorization))
                    });

                    if (authResponse == null) {
                        throw new System.ArgumentException("Failed to authenticate");
                    }

                    var token = authResponse.data.token;

                    Console.WriteLine("Currently signed in as:");
                    await credlyApi.SendGetRequest("me", new[] { new Dictionary<string, string> { { "access_token", Convert.ToString(token) } } });

                    Console.WriteLine("Enter in the Badge ID you would like to issue:");
                    var badgeId = Console.ReadLine();

                    Console.WriteLine("Enter in the email address of the recipient:");
                    var recipientEmail = Console.ReadLine();

                    Console.WriteLine("Enter in the first name of the recipient:");
                    var recipientFirst = Console.ReadLine();

                    Console.WriteLine("Enter in the last name of the recipient:");
                    var recipientLast = Console.ReadLine();

                    var issueResponse = await credlyApi.SendPostRequest(
                        "member_badges",
                        new[] {new Dictionary<string, string>{{"access_token", Convert.ToString(token)}}},
                        new[] {
                            new KeyValuePair<string, string>("badge_id", badgeId),
                            new KeyValuePair<string, string>("email", recipientEmail),
                            new KeyValuePair<string, string>("first_name", recipientFirst),
                            new KeyValuePair<string, string>("last_name", recipientLast),
                        }
                    );
                    return 0;
                });
            });

            app.Execute(args);
        }

        public static string readPassword()
        {
            string pass = "";
            ConsoleKeyInfo key;

            do {
                key = Console.ReadKey(true);

                // Backspace Should Not Work
                if (!char.IsControl(key.KeyChar)) {
                    pass = pass + key.KeyChar;
                    Console.Write("*");
                } else {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0) {
                        pass.Remove(pass.Length - 1);
                        Console.Write("\b \b");
                    }
                }
            }
            // Stops Receving Keys Once Enter is Pressed
            while (key.Key != ConsoleKey.Enter);

            Console.WriteLine("");
            return pass;
        }
    }
}
